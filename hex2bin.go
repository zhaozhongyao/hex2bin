package main

import (
	"bufio"
	"bytes"
	"crypto/md5"
	"encoding/binary"
	"encoding/hex"
	"flag"
	"fmt"
	"io"
	"os"
	"strings"
	"time"
)

type FileMode uint32

type FileInfo interface {
	Name() string       // base name of the file
	Size() int64        // length in bytes for regular files; system-dependent for others
	Mode() FileMode     // file mode bits
	ModTime() time.Time // modification time
	IsDir() bool        // abbreviation for Mode().IsDir()
	Sys() interface{}   // underlying data source (can return nil)
}

func DecodeStr(s string) int {
	src := []byte(s)
	dst := make([]byte, hex.DecodedLen(len(src)))
	hex.Decode(dst, src)

	return int(dst[0])
}

func ConvertToBin(File_Src, File_Target string) bool {
	var bdata int
	fmt.Println("-----Bin-----")
	if File_Src == "" || File_Target == "" {
		return false
	}
	if Md5File(File_Src) == "" {
		fmt.Println("Src file do not exist!")
		return false
	}
	FileInfo, _ := os.Stat(File_Src)
	fmt.Println("Src:", File_Src)
	fmt.Print("*FileSize:")
	fmt.Print(FileInfo.Size())
	fmt.Print("bytes ")
	fmt.Print("*MD5:")
	fmt.Println(Md5File(File_Src))

	fi, err := os.Open(File_Src)
	if err != nil {
		panic(err)
	}
	defer fi.Close()

	fo, err := os.Create(File_Target)
	if err != nil {
		panic(err)
	}
	defer fo.Close()

	br := bufio.NewReader(fi)
	for {
		line, err := br.ReadString('\n')
		if err == io.EOF {
			break
		} else {
			line = strings.Trim(line, " ")
			linesize := line[1:3]
			size := DecodeStr(linesize)
			linedatarec := line[7:9]
			isdatarec := DecodeStr(linedatarec)
			if isdatarec != 0 {
				continue
			}
			for h := 0; h < size; h++ {
				bdata = DecodeStr(line[9+h*2 : 9+h*2+2])
				buf := new(bytes.Buffer)
				binary.Write(buf, binary.LittleEndian, uint8(bdata))
				fo.Write(buf.Bytes())
			}
		}
	}
	fmt.Println("Target:", File_Target)

	return true
}

func ConvertToMot(File_Src, File_Target string) bool {
	fmt.Println("-----Mot-----")
	fmt.Println("Src:", File_Src)
	fmt.Println("Target:", File_Target)

	return true
}

func Md5File(path string) string {

	file, inerr := os.Open(path)
	if inerr != nil {
		return ""
	}
	md5h := md5.New()
	io.Copy(md5h, file)
	return fmt.Sprintf("%x", md5h.Sum([]byte(""))) //md5
}

func main() {
	ToFormat := flag.String("f", "bin", "target file format. now we support bin format only.")
	OverideFile := flag.Bool("o", true, "overide same name.")
	ConvertSuccess := false

	flag.Parse()
	fmt.Println(*ToFormat)
	fmt.Println(*OverideFile)
	if strings.EqualFold(*ToFormat, "bin") {
		ConvertSuccess = ConvertToBin(flag.Arg(0), flag.Arg(1))
	}
	if strings.EqualFold(*ToFormat, "mot") {
		ConvertSuccess = ConvertToMot(flag.Arg(0), flag.Arg(1))
	}

	if ConvertSuccess {
		fmt.Println("Operate success!")
	} else {
		fmt.Println("Operate failed!")
	}

}
